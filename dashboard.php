<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="dashboard.css">
    <title>Dashboard</title>
</head>
<body>
    <div class="nav_container">
        <div class="logo">
            <a href="dashboard.php"><img src="flipkart-logo.jpg" alt=""></a>
        </div>
        <!-- search button -->

        <div class="searchbar">
            <form action="" class="searchbar_form">
                <input  class="search_input" id="search_input" type="text" onkeyup="searchFunction()" placeholder="      Search for products, brands and more.. ">
                <i class="fa fa-search fa-2x"></i>
                
            </form>
        </div>

        <div class="cart">
            <a href="cart.html" class="c_logo"><i class="fa fa-cart-plus "></i></a>
        </div>

        <div class="profile">
            
            <button type="submit" id="dashboard_logout" onclick="logoutFunction()">logout</button>
        </div>

    </div>


    <div class="container">
        <form action="dashboard_ajax.php" onsubmit="productFunction()">
            <input type="number" name="product_id" id="product_id" placeholder="Product ID" />
            <input type="text" name="product_title" id="product_title" placeholder="Product Title" />
            <input type="text" name="product_description" id="product_description" placeholder="Product Description" />
            <input type="text" name="product_catagory" id="product_catagory" placeholder="Product Catagory" />
            <input type="number" name="product_price" id="product_price" placeholder="Product Price" />
            <input type="text" name="image" id="image" placeholder="image " />
            <input type="any" name="rating" id="rating" placeholder="Rating " />


            <br>
            <button type="submit" > Submit</button>
            
        </form>
    </div>

    
    <div class="heading">
        <h1 class="heading-title">Smartwatch list</h1>
    </div>
    <div class="watch_container" id="watch_container">

        

    </div>
    

    <footer class="footer">
        <p>Design by <span> Tapan Kumar Swain    </span> <i class="fa fa-copyright fa-2x"> </i> Since 1999</p>
    </footer>
    


    <script >

        var base_url = window.location.origin;

        // Search function


        function searchFunction() {
            var input, filter, cards, cardContainer, title, i;
            input = document.getElementById("search_input");
            filter = input.value.toUpperCase();
            cardContainer = document.getElementById("watch_container");
            cards = cardContainer.getElementsByClassName("card");
            for (i = 0; i < cards.length; i++) {
                title = cards[i].querySelector("#product_title");
                    if (title.innerHTML.toUpperCase().indexOf(filter) > -1) {
                         cards[i].style.display = "";
                    } else {
                        cards[i].style.display = "none";
                    }
            }
        }

        function fetchProductFromDb(callback){
            $.ajax({
                url: "fetch_product.php",
                type: "GET",
                success: function(products){
                    // console.log(JSON.parse(products));
                    callback(JSON.parse(products))
                }
            });
        }
        var productdata=[];
        
        const showproductFunction = () => {
            document.getElementById("watch_container").innerHTML="";
            let data1=[];
            const productsdbdata = fetchProductFromDb(function(products){
                
                
                productdata = Object.keys(products).map(key => {
                    return { key, ...products[key] };
                });
                console.log('tapan', typeof(productdata)); // handle the products here
                            const data=JSON.stringify(productdata);
                            localStorage.setItem("products",data);


                            products.forEach((ele,ind)  =>{
                                
                                let cardgroup = document.getElementById("watch_container");

                                let card =document.createElement("div");
                                card.id="card";
                                card.className="card";
                                cardgroup.appendChild(card);

                                let cardimage = document.createElement("a");
                                cardimage.id="card_image";
                                cardimage.className="card_image";
                                cardimage.setAttribute("onclick",`rendertoproduct(${ele.product_id})`);
                                card.appendChild(cardimage);

                                let image = document.createElement("img");
                                image.className="imgg";
                                image.src= `${ele.image}`;
                                cardimage.appendChild(image);

                                let carddetail = document.createElement("div");
                                carddetail.className="card_details";
                                card.appendChild(carddetail);

                                let h3 = document.createElement("h5");

                                let smallproductname = ele.product_title.substring(0,50);
                                h3.innerHTML=`${smallproductname}...`;
                                // h3.innerHTML= `${ele.product_title}`;
                                h3.id="product_title";
                                carddetail.appendChild(h3);

                                

                                let cardp = document.createElement("div");
                                cardp.className="card_price";
                                card.appendChild(cardp);

                                let price = document.createElement("p");
                                price.className="product_price";
                                price.append('<i class="fa fa-rupee"></i>')
                                price.innerHTML=`₹ ${ele.product_price}`;
                                cardp.appendChild(price);

                                let aprice = document.createElement("p");
                                aprice.className="after_price";
                                aprice.innerHTML="/only";
                                cardp.appendChild(aprice);

                                let delivery = document.createElement("p");
                                delivery.className="msg";
                                delivery.innerHTML="Free delivery";
                                card.appendChild(delivery);
                            });
                        });
            // let data2= productsdbdata;
            // if(data2){
            //     data1=data2;
            // }
            // console.log(data2)
            
        };
        showproductFunction();

        function rendertoproduct(productcode){
           
            localStorage.setItem('productcode',productcode);
            window.location.replace(base_url+"/e-commerce/productdetails.php");

        }

        const currentuser = JSON.parse(localStorage.getItem("loggedinuser"));
        if(!currentuser){
            window.location.replace("http://127.0.0.1:5500/login.html");
        }
        // document.getElementById("username").innerHTML = currentuser.name;


        

        const logoutFunction=()=>{
            localStorage.removeItem("loggedinuser");
            window.location.replace(base_url+"/e-commerce/login.php");

        };





        const localstoragedata= JSON.parse(localStorage.getItem("products"));
        if(localstoragedata){
            productdata=localstoragedata;
        }

        const productFunction= () => {
            event.preventDefault();

            const product_id=document.getElementById("product_id").value;
            const product_title=document.getElementById("product_title").value;
            const product_description=document.getElementById("product_description").value;
            const product_catagory=document.getElementById("product_catagory").value;
            const product_price=document.getElementById("product_price").value;
            const image=document.getElementById("image").value;
            const rating=document.getElementById("rating").value;

            document.getElementById("product_id").value="";
            document.getElementById("product_title").value ="";
            document.getElementById("product_description").value="";
            document.getElementById("product_catagory").value="";
            document.getElementById("product_price").value="";
            document.getElementById("image").value="";
            document.getElementById("rating").value="";

            const productdetails={product_id:product_id,product_title:product_title,product_description:product_description,product_catagory:product_catagory,product_price:product_price,image:image,rating:rating};
        
            productdata.push(productdetails);
            // console.log(productdata);
            const data=JSON.stringify(productdata);
            localStorage.setItem("products",data);
            // console.log(products);

            $.ajax({
                url: "dashboard_ajax.php",
                type: "POST",
                data: productdetails,
                success: function(productmsg){
                    console.log(productmsg);
                }
            });

        };
        
    </script>
</body>
</html>