

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <link rel="stylesheet" href="loginstyle.css">

    <title>Sign Up Page</title>
</head>
<body>
    <div class="main-alert">

        <div id="alert"  style="display:none; "></div>
    </div>
    <div class="maincontainer ">
        <div class="photocontainer" id="photoo">
            <img class="image_section" id="photoo_c" src="Add a heading.jpg" alt="">
        </div>
        <div class="logincontainer">
                <h3 class="login_head">Sign up</h3>
                <p class="login_description">Get access to your Orders. Wishlist and Recomendatios.</p>
                
                <form action="signup_ajax.php" method="post" id="signup" onsubmit="submitFunction()">
                    <?php if (isset($_GET['error'])) { ?>

                    <p class="error"><?php echo $_GET['error']; ?></p>

                    <?php } ?>
                    <label class="label" for="Name">User ID</label><br>
                    <input id="name" name="name" class="input_box" type="text" placeholder=""><i class="fa fa-user fa-fw" id="input_icon"></i><br>
                    <label class="label" for="Email/Mobile">Enter Email ID</label><br>
                    <input id="email" name="email" type="email" class="input_box" placeholder="" required><i class="fa fa-envelope" id="input_icon"></i><br>
                    <label class="label" for="">Enter Password</label> <br>
                    <!-- <input id="password" class="input_box" type="password"  placeholder="****************" required><br> -->
                    <input type="password" name="password" class="input_box" id="password" name="password" pattern="(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{8,16}" title="Must contain at least one number, atleast one special character and one uppercase and lowercase letter, and at least 8 to 16 characters" required>
                    <div class="button">

                        <button type="submit" class="sign">Sign up</button>
                        <a href="login.php"><button type="button" class="log">Login</button></a> 

                    </div>
                </form>

        </div>
    </div>



     <script >
        var base_url = window.location.origin;
        var demo=[];

        const udata = JSON.parse(localStorage.getItem("user"));
        // console.log(udata);
        if(udata){
            demo=udata;

        }

        const submitFunction=()=>{
            event.preventDefault();

            const name=document.getElementById("name").value;
            const email=document.getElementById("email").value;
            const password=document.getElementById("password").value;

            document.getElementById("name").value="";
            document.getElementById("email").value="";
            document.getElementById("password").value="";

            var userdata={name:name,email:email,password:password};


            if (!userdata) {
                alert("Please Enter the data:");
                // console.log("no data");
            }
             else{
                // console.log(userdata.name);
                let tname =userdata.name.trim();
                userdata={name:tname,email:email,password:password};

             }

            
            demo.push(userdata);
            // console.log(demo);

            const data=JSON.stringify(demo);
            localStorage.setItem("user",data);


            $.ajax({
		    type: "POST",
		    url: "signup_ajax.php", //call storeemdata.php to store form data
		    data: userdata,
		    success: function(echodata) {
                console.log(echodata);
                if( echodata == 1){

                        // alert("Successu");
                    var alertbox=document.getElementById('alert');
                    alertbox.innerHTML='Registration successfull';
                    alertbox.style="display:block";
                    alertbox.className="success-alert";
                    setTimeout(()=>{
        
                        alertbox.style="display:none;";
                    },5000)
                }
                else{
                    var alertbox=document.getElementById('alert');
                    alertbox.innerHTML='The User Already Exist';
                    alertbox.style="display:block";
                    alertbox.className="fail-alert";
                    setTimeout(()=>{
        
                        alertbox.style="display:none;";
                    },5000)
                }
            //   window.location.replace(base_url+"/e-commerce/login.php");

            // console.log('data',JSON.parse(data).status);
		    }
	        });

            
        };
    </script> 
</body>
</html>