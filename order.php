<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="order.css">
    <title>Cart</title>
</head>
<body>
    <div class="nav_container">
        <div class="logo">
            <a href="dashboard.php"><img src="flipkart-logo.jpg" alt=""></a>
        </div>
        <!-- search button -->

        <div class="searchbar">
            <form action="" class="searchbar_form">
                <input class="search_input" type="text" placeholder="      Search for products, brands and more.. ">
                <i class="fa fa-search fa-2x"></i>
                
            </form>
        </div>

        <!-- <div class="cart">
            <a href="cart.html" class="c_logo"><i class="fa fa-cart-plus"></i></a>
        </div> -->

        <div class="profile">
            
            <button type="submit" id="dashboard_logout" onclick="logoutFunction()">logout</button>
            
        </div>

    </div>

    <div class="main_div">
        
    </div>

    

    

    
</body>

</html>