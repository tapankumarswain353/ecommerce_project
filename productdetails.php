<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="productdetails.css">
    <title>Product Details</title>
</head>
<body>
    <div class="nav_container">
        <div class="logo">
            <a href="dashboard.php"><img src="flipkart-logo.jpg" alt=""></a>
        </div>
        <!-- search button -->

        <div class="searchbar">
            <form action="" class="searchbar_form">
                <input class="search_input" type="text" placeholder="      Search for products, brands and more.. ">
                <i class="fa fa-search fa-2x"></i>
                
            </form>
        </div>

        <div class="cart">
            <a href="cart.html" class="c_logo"><i class="fa fa-cart-plus"></i></a>
        </div>
        <div class="profile">
            
            <button id="dashboard_logout" onclick="logoutFunction()">logout</button>
        </div>

    </div>

    <div id="details">
        
    </div>


    <footer class="footer">
        <p>Design by <span> Tapan Kumar Swain    </span> <i class="fa fa-copyright fa-2x"> </i> Since 1999</p>
    </footer>

    <script defer>
        var base_url = window.location.origin;


        function isCode(){
            
            let productcode= localStorage.getItem("productcode");
            console.log(productcode);
            let product = JSON.parse(localStorage.getItem("products"));
            console.log(product);

            product.forEach((ele,ind) => {
                // console.log(typeof(ele.product_id))
                // console.log(typeof(productcode))

                if(ele.product_id === productcode)
                {
                    let productdetail= document.getElementById("details");

                    let selectedproduct = document.createElement("div");
                    selectedproduct.className="photocontainer";
                    productdetail.appendChild(selectedproduct);

                    let pimage = document.createElement("img");
                    pimage.id="img";
                    pimage.className="img";
                    pimage.src=`${ele.image}`;
                    selectedproduct.appendChild(pimage);

                    let pdcontainer = document.createElement("div");
                    pdcontainer.className="productdetailcontainer";
                    productdetail.appendChild(pdcontainer);

                    let pname = document.createElement("div");
                    pname.className="productname";
                    pdcontainer.appendChild(pname);

                    let pn = document.createElement("p");
                    pn.innerHTML=`${ele.product_title}`;
                    pn.className="product_title";
                    pname.appendChild(pn);

                    let pdescription = document.createElement("div");
                    pdescription.className="productdescription";
                    pdcontainer.appendChild(pdescription);

                    let rating = document.createElement("div");
                    rating.className="ratingcontainer";
                    pdcontainer.appendChild(rating);

                    // let prating = document.createElement("p");
                    // prating.className="productrating";
                    // rating.appendChild(prating);

                    let ratingspan = document.createElement("p");
                    ratingspan.className="rate";
                    ratingspan.innerHTML=`${ele.rating}`;
                    rating.appendChild(ratingspan);

                    let starsvg = document.createElement("img");
                    starsvg.src="icons8-star-24.png";
                    starsvg.className="icon";
                    rating.appendChild(starsvg);

                    let pprice=document.createElement("div");
                    pprice.className="productprice";
                    pdcontainer.appendChild(pprice);

                    let price = document.createElement("p");
                    price.className="price_p";
                    pprice.appendChild(price);

                    let aprice = document.createElement("p");
                    aprice.className="beforeprice";
                    aprice.innerHTML="₹";
                    pprice.appendChild(aprice);

                    let span = document.createElement("span");
                    span.innerHTML=`${ele.product_price}` + "/only";
                    span.className="price_place";
                    pprice.appendChild(span);

                    let pdes = document.createElement("div");
                    pdes.className="description";
                    pdcontainer.appendChild(pdes);

                    let description = document.createElement("p");
                    description.className="descrip";
                    description.innerHTML=`${ele.product_description}`;
                    pdes.appendChild(description);

                    let buy = document.createElement("div");
                    buy.className="buyproduct";
                    pdcontainer.appendChild(buy);

                    let cartbutton = document.createElement("button");
                    cartbutton.className="atc";
                    cartbutton.id="atc";
                    cartbutton.innerHTML="Add to Cart";
                    buy.appendChild(cartbutton);

                    let buybutton = document.createElement("button");
                    buybutton.className="bn";
                    buybutton.id="bn";
                    buybutton.innerHTML="Buy now";
                    buy.appendChild(buybutton);
                
                }
            });
        }
        isCode();

        document.getElementById("bn").setAttribute("onclick",`rendertoCart()`);
        function rendertoCart(){
            window.location.replace(base_url+"/e-commerce/cart.html");

        }

        document.getElementById("atc").setAttribute("onclick",`addtocart()`);
        function addtocart() {
            let productcode= localStorage.getItem("productcode");
            alert("Added to the cart");
            var cartarr=JSON.parse(localStorage.getItem("cart"));
            var product=JSON.parse(localStorage.getItem("products"));

            // The below if checks if cart key is there before in localstorage 
            
            if(cartarr){
                // checkvariable is used to check the presence of the current product in cart 
                checkvariable=1;
                cartarr.forEach((element,index)=>{
                    if(element.product_id===productcode){
                        checkvariable=0;
                        element.quantity = element.quantity+1;
                    }
                })
                if(checkvariable){
                    product.forEach((ele,ind) => {
                    if(ele.product_id === productcode){
                        ele.quantity=1;
                        cartarr.push(ele);
                    }
                });
                }

                localStorage.setItem("cart",JSON.stringify(cartarr));
            }
            else{
                product.forEach((ele,ind) => {
                    if(ele.product_id === productcode){
                        ele.quantity=1;
                        localStorage.setItem("cart",JSON.stringify([ele]));
                    }
                });
            }
        }



        
        
        const logoutFunction=()=>{
            localStorage.removeItem("loggedinuser");
            window.location.replace(base_url+"/e-commerce/login.php");
            

        };
        
    </script>
    
</body>
</html>