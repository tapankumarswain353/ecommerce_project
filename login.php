<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="loginstyle.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <title>Login Page</title>
</head>
<body>
    
    <div class="maincontainer ">
        <div class="photocontainer">
            
            <img class="image_section" src="Add a heading.jpg" alt="">
        </div>
        <div class="logincontainer">
                <h3 class="login_head">Login</h3>
                <p class="login_description">Get access to your Orders. Wishlist and Recomendatios.</p>
                <div class="error">
                    <p id="errormsg" style="display:none; "></p>
                </div>
                <form action="login_ajax.php" id="signup" onsubmit="myFunction()" method="post">
                    <label class="label" for="Email">Enter Email</label><br>
                    <input id="email" name="email" type="email" class="input_box" placeholder=" xyz@gmail.com" required><i class="fa fa-envelope" id="input_icon"></i><br>
                    <label class="label" for="">Enter Password</label> <br>
                    <input type="password" name="password" class="input_box" id="password"  required>
                    <!-- <input id="password" class="input_box" type="password" placeholder="****************"  required><br> -->
                    <div class="button">
                        <button type="submit" class="login">Login </button>

                        <button type="button" class="signup"><a href="signup.php">Sign up</a> </button>
                    </div>
                </form>
                <p class="last_p">We no longer support login via Social accounts. To recover your old accounts please <a href="" class="linkk">click here</a></p>

        </div>
    </div>


    <script >
        var base_url = window.location.origin;

        var exist_user=[];
        const mdata=JSON.parse(localStorage.getItem("user"));

        if(mdata)
        {
            exist_user=mdata;
        }
        // console.log(exist_user);

        const myFunction=()=>{
            event.preventDefault();

            const email= document.getElementById("email").value;
            const password= document.getElementById("password").value;
            document.getElementById("email").value="";
            document.getElementById("password").value="";

            var input_data = {email:email,password:password};

            exist_user.forEach((ele,ind) => {
                if ( ele.email===email && ele.password === password){
                    localStorage.setItem("loggedinuser", JSON.stringify(ele));
                    // console.log(ele);
                    // console.log("SUCCESS");
                    // window.location.replace("http://127.0.0.1:5500/dashboard.html");


                   
                }
            });

             $.ajax({
		            type: "POST",
		            url: "login_ajax.php", //call storeemdata.php to store form data
		            data: input_data,
		            success: function(logindata){
                    console.log(logindata);
                    if(logindata==1){

                         window.location.replace(base_url+"/e-commerce/dashboard.php");
                    }
                    else{
                       var errormsg =  document.getElementById("errormsg");
                       errormsg.className="error-message";
                       errormsg.style="display:block";

                       errormsg.innerHTML= "Please Enter Valid Email or Password !";
                       setTimeout(()=>{       
                         errormsg.style="display:none;";
                       },5000)
                    }
                    }
		
	                 });
        };

        

    </script>
</body>
</html>