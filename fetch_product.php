<?php
include "db.php";

$sql = "SELECT * FROM products";

$result = mysqli_query($conn, $sql) or die("SQL query failed.");
$productarray=[];
if (mysqli_num_rows($result) > 0) {
  while($row = mysqli_fetch_assoc($result)) {
    array_push($productarray,$row);
  }
  print_r(json_encode($productarray));
} else {
  echo "No products found.";
}

// mysqli_close($conn);
?>